﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tileGenerateScript : MonoBehaviour
{

    public GameObject[] tileSprites;

    public GameObject firstTile;

    private static tileGenerateScript instance;

    public static tileGenerateScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<tileGenerateScript>();
            }

            return instance;
        } 
            
            
    }

    // Start is called before the first frame update
    void Start()
    {
        

        for (int i = 0; i < 10; i++)
        {
            SpawnTiles();
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnTiles()
    {
        int randomCount = Random.Range(0, 2);

        firstTile = Instantiate(tileSprites[randomCount], firstTile.transform.GetChild(0).transform.GetChild(randomCount).position, Quaternion.identity);


        int spuwnCylinder = Random.Range(0, 10);

        if (spuwnCylinder == 0)
        {
            firstTile.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    public void ReloadLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    
        
}

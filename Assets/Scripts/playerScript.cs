﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
public class playerScript : MonoBehaviour
{

    public Vector3 direction;
    public float speed;
    private bool isDead;
    Rigidbody rigidbody;
    public int score = 0;
    public Text txt;

    void Start()
    {
        Debug.Log(SceneManager.GetActiveScene().name);

        isDead = false;

        direction = Vector3.zero;
    }

    
    void Update()
    {
        txt.text = "Score: " + score;

        if (Input.GetMouseButtonDown(0) && !isDead)
        {

            if (direction == Vector3.right)
            {
                direction = Vector3.forward;
            }
            else 
            {
                direction = Vector3.right;
            }
        }
        float amoutToMove = speed * Time.deltaTime;

        transform.Translate(direction * amoutToMove);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cylinder")
        {
            score += 1;
            other.gameObject.SetActive(false);
        }
    }

    private void OnMouseDown()
    {
             SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tile")
        {
            RaycastHit hit;

            Ray downRay = new Ray(transform.position, -Vector3.up);

            if (!Physics.Raycast(downRay, out hit))
            {
                isDead = true;

                OnMouseDown();

                if (transform.childCount > 0)
                {
                    transform.GetChild(0).transform.parent = null;
                }
                speed = 0;
                score = 0;

                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
        }
        

        
    }
    
}
